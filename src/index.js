import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  
  const a=parseInt(req.query.a||'0')||0;
  const b=parseInt(req.query.b||'0')||0;
  const sum=a+b;
 
  res.send(sum.toString());
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
